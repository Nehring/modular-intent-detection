# calculate decrease of accuracy as the number of domains increases

from experiment import init_experiments, run_single_experiment
import logging
import os
import json
import tracemalloc

tracemalloc.start()
args, log_dir=init_experiments("acc_per_num_domains")

args.num_epochs=1
args.n_modules=1

run_single_experiment(args, log_dir)

snapshot = tracemalloc.take_snapshot()
top_stats = snapshot.statistics('lineno')

print("[ Top 10 ]")
for stat in top_stats[:10]:
    print(stat)
