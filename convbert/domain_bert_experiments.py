
from experiment import init_experiments, run_single_experiment
import logging

def run_grid_search(args, log_dir):

    for i_try in range(3):
        for dataset in "hwu", "hwu_orig", "clinc":
            for model in ["mehri", "bert", "domain_bert"]:
                for mlm_pretraining in [True, False]:
                    
                    args.mlm_pretraining = mlm_pretraining
                    args.model = model
                    args.dataset = dataset

                    params={
                        "i_try": i_try,
                        "mlm_pretraining": mlm_pretraining,
                        "model": model,
                        "dataset": dataset
                    }
                    name=""
                    for key, value in params.items():
                        if len(name)>0:
                            name += ","
                        name += f"{key}={value}"
                    logging.info("-"*20)
                    logging.info(name)
                    logging.info("-"*20)
                    run_single_experiment(args, log_dir, experiment_name=name)

if __name__ == '__main__':

    args, log_dir=init_experiments("compare_models")

    args.n_modules=1

    args.use_observers=False
    #args.mlm_pretraining=False
    args.example=False
    run_grid_search(args, log_dir)

    # args.use_observers=True
    # args.mlm_pretraining=False
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers")

    # args.use_observers=True
    # args.mlm_pretraining=True
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers+mlm")

    # args.use_observers=True
    # args.mlm_pretraining=True
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+mlm")














