import torch
from dataloader import load_dataset
from pytorch_lightning import LightningModule, Trainer
from transformers import BertTokenizer, BertConfig, BertForSequenceClassification #, ConvBertTokenizer, ConvBertForSequenceClassification
import os
from torchmetrics import Accuracy
import sys
from bert_model import ExampleIntentBertModel, IntentBertModel, BertPretrain
import argparse
import random
import numpy as np
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from datetime import datetime
import logging
from torch.nn import NLLLoss 
from tqdm import trange, tqdm
import gc
import json
from domain_bert import DomainBert

# trainer adapted from https://pytorch-lightning.readthedocs.io/en/latest/notebooks/lightning_examples/mnist-hello-world.html
class ConvbertLightning(LightningModule):
    # task is either "domain", or "intent"
    # dataset is either "clinc" or "hwu"
    # model is either "mehri" or "..."
    def __init__(self, args, task, intent_labels, domain_labels, intent_label_ids, domain_label_ids, example_loader):

        assert task in ["domain", "intent"]

        super().__init__()

        self.args=args

        self.task=task
        
        self.intent_labels=intent_labels
        self.domain_labels=domain_labels
        self.intent_label_ids=intent_label_ids
        self.domain_label_ids=domain_label_ids

        self.example_loader=example_loader

        num_labels=len(self.intent_labels)
        if task == "domain":
            num_labels=len(self.domain_labels)

        if args.model == "mehri":            
            model_path="model/"

            if self.args.example:

                self.model = ExampleIntentBertModel(
                    model_name_or_path=model_path,
                    dropout=args.dropout,
                    use_observers=args.use_observers,
                    num_intent_labels=num_labels
                )
            else:
                self.model=IntentBertModel(
                    model_name_or_path=model_path,
                    dropout=args.dropout,
                    use_observers=args.use_observers,
                    num_intent_labels=num_labels
                )
        elif args.model == "bert":
            config=BertConfig.from_pretrained('bert-base-uncased')
            config.num_labels=num_labels
            config.attention_probs_dropout_prob=args.dropout
            config.hidden_dropout_prob=args.dropout
            self.model=BertForSequenceClassification(config)
        elif args.model == "domain_bert":
            self.model=DomainBert(args.model_path, len(self.intent_labels), len(self.domain_labels))
        elif args.model =="yitutech":
            self.tokenizer = ConvBertTokenizer.from_pretrained("YituTech/conv-bert-base")
            self.model = ConvBertForSequenceClassification.from_pretrained("YituTech/conv-bert-base")
        else:
            raise Exception("unknown model " + str(model))

        self.accuracy = Accuracy()

    def training_step(self, batch, batch_idx):

        if self.args.model == "bert":
            loss=self.model(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
                labels = batch[self.task + "_encoded"]
            ).loss
            return loss
            
        elif self.args.model == "domain_bert":
            predictions, loss=self.model(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
                intent_labels = batch["intent_encoded"],
                domain_labels = batch["domain_encoded"]
            )
            return loss

        elif self.args.model == "mehri":
            if self.args.example:

                example_input_ids, example_attention_mask, example_token_type_ids, example_true_label=self.example_loader.get_examples(batch, batch["input_ids"].device)

                # print(example_true_label.shape)
                # print(batch[self.task + "_encoded"])
                # sys.exit(0)

                intent_logits, loss=self.model.forward(
                    input_ids = batch["input_ids"],
                    attention_mask = batch["attention_mask"],
                    token_type_ids = batch["token_type_ids"],
                    intent_label = batch[self.task + "_encoded"],
                    example_input = example_input_ids,
                    example_mask = example_attention_mask,
                    example_token_types = example_token_type_ids,
                    example_intents = example_true_label
                )
                return loss
            else:
                intent_logits, loss=self.model(
                    input_ids = batch["input_ids"],
                    attention_mask = batch["attention_mask"],
                    token_type_ids = batch["token_type_ids"],
                    intent_label = batch[self.task + "_encoded"]
                )
                return loss
        else:
            raise Exception("unknown model " + args.model)

    def validation_step(self, batch, batch_idx, name="val"):    
        y_true=batch[self.task + "_encoded"]

        if self.args.model == "mehri":
            logits=self.model.encode(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
            )
        elif self.args.model == "domain_bert":
            logits, loss=self.model(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
                intent_labels = batch["intent_encoded"],
                domain_labels = batch["domain_encoded"]
            )
        elif self.args.model=="bert":
            logits=self.model(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
            ).logits
        y_pred=logits.argmax(dim=1)
        self.accuracy(y_pred, y_true)
        self.log(name + "_acc", self.accuracy, prog_bar=True)

    def test_step(self, batch, batch_idx):
        self.validation_step(batch, batch_idx, name="test")

    def predict_step(self, batch, batch_idx):

        if self.args.model=="bert":
            logits=self.model(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
            ).logits
        elif self.args.model == "domain_bert":
            logits, loss=self.model(
                input_ids = batch["input_ids"],
                attention_mask = batch["attention_mask"],
                token_type_ids = batch["token_type_ids"],
                intent_labels = batch["intent_encoded"],
                domain_labels = batch["domain_encoded"]
            )
        elif self.args.model == "mehri":
            if self.args.example:
                logits, loss=self.model.encode(
                    input_ids = batch["input_ids"],
                    attention_mask = batch["attention_mask"],
                    token_type_ids = batch["token_type_ids"]
                )
            else:
                logits, loss=self.model(
                    input_ids = batch["input_ids"],
                    attention_mask = batch["attention_mask"],
                    token_type_ids = batch["token_type_ids"]
                )

        y_pred=logits.argmax(dim=1)
        
        # convert int labels to str labels
        y_pred=list(y_pred)
        if self.task=="domain":
            y_pred=[self.domain_labels[int(x)] for x in y_pred]
        elif self.task=="intent":
            y_pred=[self.intent_labels[int(x)] for x in y_pred]

        return y_pred

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.args.learning_rate, eps=self.args.adam_epsilon)
        return optimizer

# prepare inputs for mlm pretraining
def mask_tokens(inputs, tokenizer, mlm_probability=0.15):

    device=inputs.device

    """ Prepare masked tokens inputs/labels for masked language modeling: 80% MASK, 10% random, 10% original. """
    labels = inputs.clone()
    # We sample a few tokens in each sequence for masked-LM training (with probability args.mlm_probability defaults to 0.15 in Bert/RoBERTa)
    probability_matrix = torch.full(labels.shape, mlm_probability).to(device)
    # special_tokens_mask = [tokenizer.get_special_tokens_mask(val, already_has_special_tokens=True) for val in labels.tolist()]
    probability_matrix.masked_fill_(torch.tensor(labels == 0, dtype=torch.bool), value=0.0)

    masked_indices = torch.bernoulli(probability_matrix).bool().to(device)
    labels[~masked_indices] = -100  # We only compute loss on masked tokens

    # 80% of the time, we replace masked input tokens with tokenizer.mask_token ([MASK])
    indices_replaced = torch.bernoulli(torch.full(labels.shape, 0.8)).bool().to(device) & masked_indices
    inputs[indices_replaced] = tokenizer.token_to_id("[MASK]")

    # 10% of the time, we replace masked input tokens with random word
    indices_random = torch.bernoulli(torch.full(labels.shape, 0.5)).bool().to(device) & masked_indices & ~indices_replaced
    random_words = torch.randint(tokenizer.get_vocab_size(), labels.shape, dtype=torch.long)
    inputs[indices_random] = random_words[indices_random].cuda()

    # The rest of the time (10% of the time) we keep the masked input tokens unchanged
    return inputs, labels

# MLM Pre-train
def mlm_pretrain(args, mlm_dataloader, tokenizer):
    pre_model = BertPretrain("model/")
    mlm_optimizer = torch.optim.AdamW(pre_model.parameters(), lr=args.learning_rate, eps=args.adam_epsilon)
    pre_model.to(args.device)

    # Maintain most recent score per label.
    for epoch in trange(3, desc="Pre-train Epochs"):
        pre_model.train()
        epoch_loss = 0
        num_batches = 0
        for batch in tqdm(mlm_dataloader):
            num_batches += 1

            # Train model
            inputs, labels = mask_tokens(batch["input_ids"].to(args.device), tokenizer)

            loss = pre_model(inputs, labels)
            # if args.grad_accum > 1:
            #     loss = loss / args.grad_accum
            loss.backward()
            epoch_loss += loss.item()

            # if args.grad_accum <= 1 or num_batches % args.grad_accum == 0:
            #     if args.max_grad_norm > 0:
            #         torch.nn.utils.clip_grad_norm_(pre_model.parameters(), args.max_grad_norm)

            mlm_optimizer.step()
            pre_model.zero_grad()

        logging.info("Epoch loss: {}".format(epoch_loss / num_batches))

    return pre_model

def training_run(task, domains, args, log_dir):

    logging.info(f"start training run on task {task}, domains {domains}")

    # load data
    if domains==None:
        # in this case subsample the list of domains
        inputfile=os.path.join(args.dataset_path, args.dataset + "_domains.json")
        f=open(inputfile, "r")
        domains=json.load(f)
        f.close()

    if args.num_domains>0:
        random.shuffle(domains)
        domains=domains[0:args.num_domains]

    x=load_dataset(args, task, domains, shuffle_train=True, subsample=args.subsample)
    train_dataloader, valid_dataloader, test_dataloader, mlm_dataloader, example_loader, intent_labels, domain_labels, intent_label_ids, domain_label_ids, tokenizer, df=x

    # pretrain mlm model
    if args.mlm_pretraining:
        logging.info("start mlm pretraining")
        pre_model=mlm_pretrain(args, mlm_dataloader, tokenizer)
        # Transfer BERT weights

    # start training
    logging.info("start fine tuning")

    model = ConvbertLightning(args, task, intent_labels, domain_labels, intent_label_ids, domain_label_ids, example_loader)
    if args.mlm_pretraining:
        model.bert_model = pre_model.bert_model.bert
        del pre_model

    logger = TensorBoardLogger(log_dir, name="tb_logs")

    trainer = Trainer(
        gpus=min(1, torch.cuda.device_count()),
        max_epochs=args.num_epochs,
        logger=logger,
        callbacks=[EarlyStopping(monitor="val_acc", mode="max", min_delta=0.005)]
    )

    trainer.fit(model, train_dataloader, valid_dataloader)

    # predict
    y_pred=trainer.predict(model, dataloaders=test_dataloader)

    # flatten list
    y_pred=[item for sublist in y_pred for item in sublist]

    # collect true labels from dataloader
    y_true=[]
    domain_mask=[] # true for each sample in the domain list, else false
    for row in test_dataloader:
        y_true.extend(row[task])
        for domain in row["domain"]:
            domain_mask.append(domain in domains)

    del trainer
    del model
    del train_dataloader
    del valid_dataloader
    del test_dataloader
    del mlm_dataloader
    del example_loader
    del intent_labels
    del domain_labels
    del intent_label_ids
    del domain_label_ids
    del tokenizer
    del logger
    del df

    # fix weird memory issue: https://github.com/pandas-dev/pandas/issues/2659
    gc.collect()

    return y_true, y_pred, domain_mask