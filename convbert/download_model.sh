#!/bin/sh

# download mehris convbert model

mkdir model
cd model

baseurl="https://dialoglue.s3.amazonaws.com/convbert"
wget "$baseurl/config.json"
wget "$baseurl/pytorch_model.bin"
wget "$baseurl/special_tokens_map.json"
wget "$baseurl/tokenizer_config.json"
wget "$baseurl/vocab.txt"
