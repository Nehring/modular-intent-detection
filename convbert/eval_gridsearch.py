import os
import pandas as pd

logfolder="logs"
#for run in os.listdir(logfolder):
for run in ["2022-02-03T12-13-55_hwu_dataset=hwu, lr=5e-05, batch_size=128"]:
    
    params=run[len("2022-02-03T12-13-55_hwu_"):]
    df=pd.read_csv(os.path.join(logfolder, run, "results.csv"))
    print(df)