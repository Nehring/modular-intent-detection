# make commands for hyperparameter grid search on hwu dataset

# python3 make_grid_search_commands.py  > commands.txt

dataset="hwu"
for learning_rate in [4e-5, 5e-5, 6e-5]:
    for batch_size in [32, 64, 128]:
        for dropout in [0.0, 0.3, 0.5]:
            batch_size=str(batch_size)
            learning_rate=str(learning_rate)
            dropout=str(dropout)

            name=f"batch_size={batch_size},lr={learning_rate},dataset={dataset},dropout={dropout}"

            path="/netscratch/nehring/projects/modular-intent-detection/test/convbert_test/slurm/experiment_params.sh"
            base_command=f"screen -d -m -S {name} ./usrun.sh -p batch --gpus=1 --time 18:00:00"

            command=f"{base_command} {path} {batch_size} {learning_rate} {dataset} {dropout}"
            print(command)