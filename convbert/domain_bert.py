import torch
from transformers import BertModel
from torch import nn
from torch.nn import CrossEntropyLoss, NLLLoss 
import logging

class DomainBert(torch.nn.Module):
    def __init__(self,
                 model_name_or_path: str,
                 num_intent_labels: int,
                 num_domain_labels: int):
        super().__init__()
        self.bert_model = BertModel.from_pretrained(model_name_or_path)
        
        self.num_domain_labels=num_domain_labels
        self.num_intent_labels=num_intent_labels

        self.domain_layer=nn.Linear(self.bert_model.config.hidden_size, num_domain_labels)
        self.intent_layer=nn.Linear(self.bert_model.config.hidden_size+num_domain_labels, num_intent_labels)

        self.weight_domain=0.5
        self.weight_intent=1-self.weight_domain
        
    def forward(self, 
                input_ids: torch.tensor,
                attention_mask: torch.tensor,
                token_type_ids: torch.tensor,
                domain_labels: None,
                intent_labels: None):

        outputs = self.bert_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)

        domain_outputs=self.domain_layer(outputs.pooler_output)
        intent_inputs=torch.cat((domain_outputs, outputs.pooler_output), dim=1)
        intent_outputs=self.intent_layer(intent_inputs)

        loss=0
        if domain_labels != None and intent_labels != None:
            loss_fct = CrossEntropyLoss()
            batch_size=len(input_ids)

            target_intent=torch.nn.functional.one_hot(intent_labels, self.num_intent_labels).float()
            target_domain=torch.nn.functional.one_hot(domain_labels, self.num_domain_labels).float()

            domain_loss = loss_fct(domain_outputs, target_domain)
            intent_loss = loss_fct(intent_outputs, target_intent)

            loss=self.weight_domain*domain_loss + self.weight_intent*intent_loss

        return intent_outputs, loss
