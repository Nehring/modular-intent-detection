# calculate decrease of accuracy as the number of domains increases

from experiment import init_experiments, run_single_experiment
import logging
import os
import json

if __name__ == '__main__':

    args, log_dir=init_experiments("acc_per_num_domains")

    args.use_observers=False
    args.mlm_pretraining=False
    args.example=False
    args.n_modules=1

    for dataset in ["hwu"]:
        
        f=open(os.path.join(args.dataset_path, dataset + "_domains.json"), "r")
        max_n_domains=len(json.load(f))
        f.close()

        for n_domains in range(1, max_n_domains):

            args.num_domains=n_domains
            
            for i in range(5):
                experiment_name=f"hwu,num_domains={n_domains},try={i}"
                logging.info("-"*20)
                logging.info("starting experiment " + experiment_name)
                logging.info("-"*20)
                run_single_experiment(args, log_dir, experiment_name=experiment_name)
