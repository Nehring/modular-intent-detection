# run a single model training and evaluation

import os
import argparse
import random
from datetime import datetime
from convbert_lightning import training_run
import logging
from sklearn.metrics import accuracy_score
import pandas as pd
from dataloader import load_domain_assignment
import numpy as np
import torch
from time import time
import sys

# create command line argument parser and the default values
def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_path", type=str, default="model")
    parser.add_argument("--use_observers", action="store_true")
    parser.add_argument("--subsample", type=int, default=-1, help="Set to a value>0 to reduce the size of the datasets to this value.")
    parser.add_argument("--log_folder", type=str, default="logs")
    parser.add_argument("--train_batch_size", type=int, default=32)
    parser.add_argument("--mlm_train_batch_size", type=int, default=32)
    parser.add_argument("--test_batch_size", type=int, default=512)
    parser.add_argument("--max_seq_length", type=int, default=40)
    parser.add_argument("--num_epochs", type=int, default=10)
    parser.add_argument("--example", action="store_true")
    parser.add_argument("--subsample_train_ten", action="store_true", help="subsample training data to 10 samples per intent in training")
    parser.add_argument("--mlm_pretraining", action="store_true")
    parser.add_argument("--num_domains", type=int, default=-1, help="Set to a value>0 to limit training to a number of domains.")
    parser.add_argument("--do_lowercase", action="store_true")
    parser.add_argument("--dropout", type=float, default=0.0)
    parser.add_argument("--learning_rate", default=5e-5, type=float, help="The initial learning rate for Adam.")
    parser.add_argument("--adam_epsilon", default=1e-8, type=float, help="Epsilon for Adam optimizer.")
    parser.add_argument("--device", default=0, type=int, help="GPU device #")
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--n_modules", type=int, default=3)
    parser.add_argument("--model", type=str, default="mehri", help="model (mehri or yitutech)")
    parser.add_argument("--dataset", type=str, default="hwu", help="dataset (hwu, hwu_orig or clinc)")
    parser.add_argument("--dataset_path", type=str, default="../dataset_generation/dataset/", help="dataset (hwu or clinc)")
    parser.add_argument("--experiment_name", type=str, default="", help="postfix for the output folder")

    args=parser.parse_args()

    assert args.model in ["mehri", "bert", "domain_bert"]
    assert args.dataset in ["clinc", "hwu", "hwu_orig"]

    return args

# create the output folder
def make_log_folder(args, name):
    if not os.path.exists(args.log_folder):
        os.mkdir(args.log_folder)

    my_date = datetime.now()

    folder_name=my_date.strftime('%Y-%m-%dT%H-%M-%S') + "_" + name

    if len(args.experiment_name) > 0:
        folder_name += "_" + args.experiment_name

    log_folder=os.path.join(args.log_folder, folder_name)
    os.mkdir(log_folder)
    return log_folder

# write a single store to the results logfile
results_df=None
def write_results(log_dir, experiment_name, name, metric):

    logging.info(f"write result row name={name}, metric={metric}")
    results_file=os.path.join(log_dir, "results.csv")

    global results_df
    if results_df is None:
        results_df=pd.DataFrame(columns=["experiment", "name", "metric"])
    
    data=[experiment_name, name, metric]
    results_df.loc[len(results_df)]=data

    results_df.to_csv(results_file)

# log to file and console
def create_logger(log_dir):
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)s] {%(filename)s:%(lineno)d} %(message)s")
    rootLogger = logging.getLogger()

    fileHandler = logging.FileHandler(os.path.join(log_dir, "log.txt"))
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    rootLogger.setLevel(logging.INFO)

# set all random seeds
def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)

# get final intent predictions by the predictions of the domain selector and the predictions of the single intent predictors
# then compute accuracy of intent prediction
def compute_accuracy(y_pred_domain, y_true_intent, y_pred_intent, domain_assignment):

    assert len(y_pred_domain) == len(y_true_intent)
    for i in range(len(y_pred_intent)):
        assert len(y_pred_intent[i]) == len(y_pred_domain)

    # mapping from domain to index in y_pred_intent
    domain_id={}
    for i in range(len(domain_assignment)):
        for domain in domain_assignment[i]:
            domain_id[domain]=i

    y_pred_intent_final=[]
    for i in range(len(y_pred_domain)):
        domain=domain_id[y_pred_domain[i]]
        intent=y_pred_intent[domain][i]
        y_pred_intent_final.append(intent)
    
    acc=accuracy_score(y_true_intent, y_pred_intent_final)
    return acc

def run_single_experiment(args, log_dir, experiment_name=""):
    try:
        start_time=time()

        logging.info("starting experiment")
        logging.info("log to " + log_dir)
        logging.info(f"arguments {args}")

        if args.model == "domain_bert" and args.n_modules>1:
            raise Exception("domain_bert cannot do domain detection.")
        
        # train domain selector
        if args.n_modules>1:
            logging.info("train domain selector")
            domains=None
            task="domain"
            y_true_domain, y_pred_domain, domain_mask=training_run(task, domains, args, log_dir)
            acc_ms=accuracy_score(y_true_domain, y_pred_domain)
            write_results(log_dir, experiment_name, "acc_ms", acc_ms)

        # train intent predictors
        y_pred_intent=[[] for i in range(args.n_modules)]
        domain_assignment=load_domain_assignment(args)

        #logging.info(domain_assignment)
        for i_module in range(args.n_modules):
            logging.info(f"train intent detector {i_module+1}/{args.n_modules}")
            domains=domain_assignment[i_module]
            task="intent"
            y_true_intent, y_pred, domain_mask=training_run(task, domains, args, log_dir)
            y_pred_intent[i_module]=y_pred

            y_pred_id=[]
            y_true_id=[]
            for i in range(len(y_pred)):
                if domain_mask[i]:
                    y_pred_id.append(y_pred[i])
                    y_true_id.append(y_true_intent[i])

            acc_id=accuracy_score(y_true_id, y_pred_id)
            write_results(log_dir, experiment_name, f"acc_id_{i_module}", acc_id)
            # this accuracy is not accurate because it includes also samples from other domains
            logging.info(f"accuracy of intent detector {i_module+1}/{args.n_modules}={acc_id}")

        # compute final accuracy score
        if args.n_modules>1:
            acc=compute_accuracy(y_pred_domain, y_true_intent, y_pred_intent, domain_assignment)
            logging.info(f"final accuracy of intent detection: {acc}")
            write_results(log_dir, experiment_name, "acc_id_total", acc)
        else:
            write_results(log_dir, experiment_name, f"acc_id_total", acc_id)

        duration=time()-start_time
        write_results(log_dir, experiment_name, "duration", duration)
    except Exception as e:
        logging.exception(e)

def init_experiments(experiment_name):
    args=read_args()
    log_dir=make_log_folder(args, experiment_name)
    create_logger(log_dir)
    set_seed(args.seed)
    log_command()
    return args, log_dir

def log_command():
    command=" ".join(sys.argv)
    logging.info("start command: " + command)

'''
test configuration
python experiment.py --num_epochs=1 --train_batch_size 128 --subsample=500

live configuration
python experiment.py
'''
if __name__ == '__main__':
    args, log_dir=init_experiments("single_experiment")
    run_single_experiment(args, log_dir)

