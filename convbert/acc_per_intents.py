from experiment import init_experiments, run_single_experiment
import logging

def run_experiments_all_datasets(args, log_dir, experiment_name):

    for dataset in ["hwu", "clinc"]:
        logging.info("-"*20)
        logging.info(txt)
        logging.info("-"*20)

        args.n_modules=3
        run_single_experiment(args, log_dir, experiment_name=experiment_name)

if __name__ == '__main__':

    args, log_dir=init_experiments()

    args.use_observers=False
    args.mlm_pretraining=False
    args.example=False
    run_experiments_all_datasets(args, log_dir, experiment_name="vanilla-convbert")

    args.use_observers=True
    args.mlm_pretraining=False
    run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers")

    args.use_observers=True
    args.mlm_pretraining=True
    run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers+mlm")

    args.use_observers=True
    args.mlm_pretraining=True
    run_experiments_all_datasets(args, log_dir, experiment_name="convbert+mlm")














