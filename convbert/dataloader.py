from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import pandas as pd
import os
import numpy as np
import sys
import json
import random
import torch
import logging
from tokenizers import BertWordPieceTokenizer
from transformers import BertTokenizer
import threading

# abstraction for the tokenization because BertTokenizer and BertWordPieceTokenizer work different
def tokenize(args, utterance, tokenizer):
    if type(tokenizer) == BertTokenizer:
        encoded = tokenizer(utterance, padding="max_length", max_length=args.max_seq_length, truncation=True)
        for key in ["input_ids", "attention_mask", "token_type_ids"]:
            encoded[key]=torch.LongTensor((encoded[key]))
        return encoded["input_ids"], encoded["attention_mask"], encoded["token_type_ids"]
    elif type(tokenizer) == BertWordPieceTokenizer:
        encoded = tokenizer.encode(utterance)
        input_ids=torch.LongTensor(encoded.ids)[-args.max_seq_length:]
        attention_mask=torch.LongTensor(encoded.attention_mask)[-args.max_seq_length:]
        token_type_ids=torch.LongTensor(encoded.type_ids)[-args.max_seq_length:]
        return input_ids, attention_mask, token_type_ids

# domains is a filter which domains to use. set to None to use all domains
# dataset can be either train, test or valid
# use subsample to speed up processing by subsampling.
def load_dataset(args, task, domains, shuffle_train=True, subsample=-1):
    
    if args.model =="mehri" or args.model =="domain_bert":
        tokenizer = BertWordPieceTokenizer(os.path.join(args.model_path, "vocab.txt"), lowercase=args.do_lowercase)
        tokenizer.enable_padding(length=args.max_seq_length)
    elif args.model == "bert":
        tokenizer = BertTokenizer.from_pretrained("bert-base-uncased", model_max_length=args.max_seq_length)
    else:
        raise Exception(f"unknown model {args.model}")

    f=os.path.join(args.dataset_path, args.dataset + ".csv")
    df=pd.read_csv(f)
    df=df.rename(columns={"Unnamed: 0": "id"})

    train_dataset=PandasDataset(args, task, tokenizer, df, domains, "train", subsample=subsample, subsample_train_ten=args.subsample_train_ten)
    valid_dataset=PandasDataset(args, task, tokenizer, df, domains, "val", subsample=subsample)
    test_dataset=PandasDataset(args, task, tokenizer, df, None, "test", subsample=subsample)

    example_loader=None
    if args.example:
        example_loader=ExampleLoader(tokenizer, train_dataset.df, domains, task, args, train_dataset.intent_labels, train_dataset.domain_labels, train_dataset.intent_label_ids, train_dataset.domain_label_ids)

    num_workers=0
    train_dataloader=DataLoader(train_dataset, batch_size=args.train_batch_size, shuffle=shuffle_train, num_workers=num_workers)
    valid_dataloader=DataLoader(valid_dataset, batch_size=args.test_batch_size, shuffle=False, num_workers=num_workers)
    test_dataloader=DataLoader(test_dataset, batch_size=args.test_batch_size, shuffle=False, num_workers=num_workers)

    mlm_dataloader=DataLoader(train_dataset, batch_size=args.mlm_train_batch_size, shuffle=shuffle_train, num_workers=num_workers)

    intent_labels=train_dataset.intent_labels
    domain_labels=train_dataset.domain_labels
    intent_label_ids=train_dataset.intent_label_ids
    domain_label_ids=train_dataset.domain_label_ids

    return train_dataloader, valid_dataloader, test_dataloader, mlm_dataloader, example_loader, intent_labels, domain_labels, intent_label_ids, domain_label_ids, tokenizer, df

class ExampleLoader():

    def __init__(self, tokenizer, df, domains, task, args, intent_labels, domain_labels, intent_label_ids, domain_label_ids):
        self.tokenizer=tokenizer
        self.task=task
        self.df=df
        self.args=args

        self.intent_labels=intent_labels
        self.domain_labels=domain_labels
        self.intent_label_ids=intent_label_ids
        self.domain_label_ids=domain_label_ids

    def get_examples(self, batch, device):

        example_utterances=[]
        example_labels=[]
        example_ids=[]

        # load examples with the same label
        all_other_items=self.df[self.df.id.apply(lambda x : x not in batch["df_id"])]
        if len(all_other_items)>0:
            for i in range(len(batch["df_id"])):
                
                subdf=all_other_items[all_other_items[self.task]==batch[self.task][i]]
                if len(subdf)==0:
                    continue

                example=subdf.sample(n=1).iloc[0]
                example_utterances.append(example["text"])
                example_labels.append(example[self.task])
                example_ids.append(example["id"])

        # load examples with other labels
        all_other_items=all_other_items[all_other_items.id.apply(lambda x : x not in example_ids)]
        if len(all_other_items)>0:
            for i in range(len(batch["df_id"])):
                
                subdf=all_other_items[all_other_items[self.task]!=batch[self.task][i]]
                if len(subdf)==0:
                    continue

                example=subdf.sample(n=1).iloc[0]
                example_utterances.append(example["text"])
                example_labels.append(example[self.task])
                example_ids.append(example["id"])

        # encode examples 
        example_input_ids=[]
        example_attention_mask=[]
        example_token_type_ids=[]
        example_true_label=[]
        for i in range(len(example_ids)):

            input_ids, attention_mask, token_type_ids = tokenize(self.args, example_utterances[i], self.tokenizer)

            if self.task=="intent":
                label_encoded=self.intent_label_ids[example_labels[i]]
            elif self.task=="domain":
                label_encoded=self.domain_label_ids[example_labels[i]]
            
            example_input_ids.append(input_ids)
            example_attention_mask.append(attention_mask)
            example_token_type_ids.append(token_type_ids)
            example_true_label.append(label_encoded)

        example_input_ids=torch.stack(example_input_ids, dim=0).to(device)
        example_attention_mask=torch.stack(example_attention_mask, dim=0).to(device)
        example_token_type_ids=torch.stack(example_token_type_ids, dim=0).to(device)

        example_true_label=torch.LongTensor(example_true_label).to(device)
        return example_input_ids, example_attention_mask, example_token_type_ids, example_true_label

class PandasDataset(Dataset):

    def __init__(self, args, task, tokenizer, df, domains, dataset, subsample=-1, subsample_train_ten=False):

        self.max_seq_length=args.max_seq_length
        self.tokenizer=tokenizer
        self.args=args
        self.task=task
        self.subsample_train_ten=subsample_train_ten
        self.lock=threading.Lock()

        self.df=df

        # filter for domains
        if domains is not None:
            self.df=self.df[self.df.domain.apply(lambda x : x in domains)]

        # filter for dataset
        self.df=self.df[self.df.dataset==dataset]

        self.intent_labels=list(self.df.intent.unique())
        self.intent_label_ids={}
        for l in self.df.intent.unique():
            self.intent_label_ids[l]=len(self.intent_label_ids)

        self.domain_labels=list(self.df.domain.unique())
        self.domain_label_ids={}
        for l in self.df.domain.unique():
            self.domain_label_ids[l]=len(self.domain_label_ids)
        
        # subsample train@10
        if subsample_train_ten:
            dfs=[]
            for intent in self.df.intent.unique():
                dfs.append(self.df[self.df.intent==intent].sample(n=10))
            self.df=pd.concat(dfs)

        # subsample
        if subsample>0:
            self.df=self.df[0:subsample]

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):

        try:
            self.lock.acquire()
            row=self.df.iloc[idx]
        finally:
            self.lock.release()

        input_ids, attention_mask, token_type_ids = tokenize(self.args, row["text"], self.tokenizer)

        sample={
            "idx": idx,
            "df_id": row["id"],
            "text": row["text"],
            "intent": row["intent"],
            "domain": row["domain"],
            "intent_encoded": self.intent_label_ids[row["intent"]],
            "domain_encoded": self.domain_label_ids[row["domain"]],
            "input_ids": input_ids,
            "attention_mask": attention_mask,
            "token_type_ids": token_type_ids
        }

        # if self.args.example:
        #     sample=self.get_examples(sample)

        return sample

# load the assignment of domains to dataset
# n_modules determines on how many modules to split the domains 
def load_domain_assignment(args):

    inputfile=os.path.join(args.dataset_path, args.dataset + "_domains.json")
    f=open(inputfile, "r")
    domains=json.load(f)
    f.close()

    random.shuffle(domains)
    if args.num_domains>0:
        domains=domains[0:args.num_domains]

    domain_assignment=[[] for i in range(args.n_modules)]
    for i in range(len(domains)):
        domain_assignment[i%args.n_modules].append(domains[i])
    
    return domain_assignment

# main function for testing only
if __name__ == '__main__':
    
    print(load_domain_assignment("hwu", 3))
    print(load_domain_assignment("hwu", 3))