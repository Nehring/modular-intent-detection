import argparse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os

def get_file(f):
    outfolder="output"
    if not os.path.exists(outfolder):
        mkdir(outfolder)
    return os.path.join(outfolder, f)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", type=str, default="../logs/2022-02-06T17-21-55_acc_per_num_domains/results.csv")
    args=parser.parse_args()

    df=pd.read_csv(args.infile)

    df=df[df.name=="acc_id_0"].rename(columns={"metric": "accuracy"})

    num_domains=[]
    tries=[]
    for e in df.experiment:
        i=e.rfind(",")
        num_domains.append(int(e[len("hwu,num_domains="):i]))
        tries.append(int(e[i+5:]))

    df["num_domains"]=num_domains
    df["tries"]=tries

    df=df[df.num_domains>1]

    sns.lineplot(data=df, x="num_domains", y="accuracy")
    f=get_file("acc_per_domains.png")
    plt.savefig(f)
    print("wrote file to " + f)