import argparse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import numpy as np

def get_file(f):
    outfolder="output"
    if not os.path.exists(outfolder):
        mkdir(outfolder)
    return os.path.join(outfolder, f)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", type=str, default="../logs/2022-02-07T12-42-22_grid_search/results.csv")
    args=parser.parse_args()

    df1=pd.read_csv(args.infile)
    df1=df1[df1.name=="acc_id_total"]
    df2={
        "accuracy": []
    }

    for ix, row in df1.iterrows():
        params={}
        for p in row["experiment"].split(","):

            name, value=p.split("=")
            if name not in df2.keys():
                df2[name]=[]
            df2[name].append(value)
        df2["accuracy"].append(row["metric"])
        
    df2=pd.DataFrame(df2)

    # aggregate over tries
    df3=[]
    df2["id"]=df2.batch_size + "_" + df2.dropout + "_" + df2.learning_rate

    for id in df2.id.unique():
        df_sub=df2[df2.id==id]
        r=df_sub.iloc[0]
        df3.append([np.mean(df_sub.accuracy), np.std(df_sub.accuracy), r["batch_size"], r["learning_rate"], r["dropout"]])

    df3=pd.DataFrame(df3, columns=["accuracy", "std", "batch_size", "learning_rate", "dropout"])


    print("all results")
    print(df3)

    print()
    print("best result")
    maxi=df3.accuracy.idxmax()
    print(df3.iloc[maxi])
