import argparse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
from acc_num_domains import get_file

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", type=str, default="../logs/2022-02-06T17-21-44_all_experiments/results.csv")
    args=parser.parse_args()

    df=pd.read_csv(args.infile)

    model=[]
    dataset=[]

    for ix, row in df.iterrows():
        e=row["experiment"]
        i=e.find("_")
        model.append(e[0:i])
        dataset.append(e[i+1:])

    df["model"]=model
    df["dataset"]=dataset

    # duration chart
    df_duration=df[df.name=="duration"].rename(columns={"metric": "duration"})
    df_duration.duration/=60 # convert to min
    sns.barplot(data=df_duration, x="model", y="duration", hue="dataset")
    f=get_file("duration.png")
    plt.savefig(f)
    print("wrote duration image to " + f)

    # acc chart
    df_acc=df[df.name=="acc_id_total"].rename(columns={"metric": "accuracy"})
    sns.barplot(data=df_acc, x="model", y="accuracy", hue="dataset")
    f=get_file("model_accuracy.png")
    plt.savefig(f)
    print("wrote model accuracy image to " + f)

    df_acc=df_acc[["model", "dataset", "accuracy"]]
    df_acc.accuracy=df_acc.accuracy.apply(lambda x : f"{x:.4f}")
    print(df_acc)