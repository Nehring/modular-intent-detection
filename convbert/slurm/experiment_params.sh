#!/bin/sh

# ./usrun.sh -p RTX2080Ti --gpus=1 --time 18:00:00 /netscratch/nehring/projects/modular-intent-detection/test/convbert_test/slurm/experiment.sh 

export TORCH_HOME=/netscratch/nehring/cache/torch
export PIP_CACHE_DIR=/netscratch/nehring/cache/pip

pip install --upgrade pip
pip install "torchvision" "torch>=1.6, <1.9" "pytorch-lightning>=1.3" "torchmetrics>=0.3"
pip install transformers==2.1.1
pip install tokenizers==0.11.4

cd /netscratch/nehring/projects/modular-intent-detection/repo/test/convbert_test

python experiment.py --train_batch_size $1 --learning_rate $2 --dataset $3 --dropout $4