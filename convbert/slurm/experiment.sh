#!/bin/sh

# ./usrun.sh -p RTX2080Ti --gpus=1 --time 18:00:00 /netscratch/nehring/projects/modular-intent-detection/repo/convbert/slurm/experiment.sh

export TORCH_HOME=/netscratch/nehring/cache/torch
export PIP_CACHE_DIR=/netscratch/nehring/cache/pip

pip install --upgrade pip
pip install transformers==4.16.2
pip install pytorch-lightning==1.5.9
pip install seaborn==0.11.2

cd /netscratch/nehring/projects/modular-intent-detection/repo/convbert

if [ $1 = "single" ]; then
    python experiment.py \
        --use_observers \
        --n_modules 1  \
        --train_batch_size 64 \
        --mlm_pretraining \
        --max_seq_length 50 
elif [ $1 = "all" ]; then
    python run_all_experiments.py 
elif [ $1 = "acc_per_num_domains" ]; then
    python experiment_acc_per_num_domains.py
elif [ $1 = "modify_n_modules_bert" ]; then
    python modify_n_modules.py --model bert
elif [ $1 = "modify_n_modules_convbert" ]; then
    python modify_n_modules.py --model mehri
elif [ $1 = "modify_n_modules_convbert_mlm" ]; then
    python modify_n_modules.py --model mehri --mlm_pretraining
elif [ $1 = "modify_n_modules_convbert_mlm_observer" ]; then
    python modify_n_modules.py --model mehri --mlm_pretraining --use_observers
elif [ $1 = "tune_parameters_bert" ]; then
    python grid_search.py --model bert
elif [ $1 = "domain_bert_experiments" ]; then
    python domain_bert_experiments.py
fi
# python experiment.py --use_observers --n_modules 1  --train_batch_size 64 --mlm_pretraining --max_seq_length 50 