# train and evaluate convbert, convbert+observers, convbert + mlm pretraining, convbert + oberservers + mlm pretraining
# for all datasets

from experiment import init_experiments, run_single_experiment
import logging

def run_grid_search(args, log_dir):

    for i_try in range(3):
        # for batch_size in [32, 128,512]:
        #     for dropout in [0, 0.25, 0.5]:
        #         for learning_rate in [1e-5, 3e-5, 5e-5]:
        for batch_size in [8,16,32]:
            for dropout in [0]:
                for learning_rate in [3e-5]:
                    args.train_batch_size=batch_size
                    args.dropout=dropout
                    args.learning_rate=learning_rate

                    params={
                        "i_try": i_try,
                        "batch_size": batch_size,
                        "dropout": dropout,
                        "learning_rate": learning_rate
                    }
                    name=""
                    for key, value in params.items():
                        if len(name)>0:
                            name += ","
                        name += f"{key}={value}"
                    logging.info("-"*20)
                    logging.info(name)
                    logging.info("-"*20)
                    run_single_experiment(args, log_dir, experiment_name=name)

if __name__ == '__main__':

    args, log_dir=init_experiments("grid_search")

    args.dataset="hwu"
    args.n_modules=1
    args.model="mehri"

    args.use_observers=False
    args.mlm_pretraining=False
    args.example=False
    run_grid_search(args, log_dir)

    # args.use_observers=True
    # args.mlm_pretraining=False
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers")

    # args.use_observers=True
    # args.mlm_pretraining=True
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers+mlm")

    # args.use_observers=True
    # args.mlm_pretraining=True
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+mlm")














