# train and evaluate convbert, convbert+observers, convbert + mlm pretraining, convbert + oberservers + mlm pretraining
# for all datasets

from experiment import init_experiments, run_single_experiment
import logging

def run_experiments_all_datasets(args, log_dir, experiment_name):

    # for dataset in ["hwu", "clinc", "hwu_orig"]:
    for n_modules in [1,5,18]:
        for dataset in ["hwu"]:
            args.n_modules=n_modules
            name = f"{experiment_name},n_modules={n_modules},dataset={dataset}"
            logging.info("-"*20)
            logging.info(experiment_name)
            logging.info("-"*20)
            run_single_experiment(args, log_dir, experiment_name=name)

            # dname=dataset + "@10"
            # name = f"{experiment_name},n_modules={n_modules},dataset={dname}"
            # logging.info("-"*20)
            # logging.info(experiment_name)
            # logging.info("-"*20)
            # args.subsample_train_ten=True
            # run_single_experiment(args, log_dir, experiment_name=name)

if __name__ == '__main__':

    args, log_dir=init_experiments("all_experiments")

    args.use_observers=False
    args.mlm_pretraining=False
    args.example=False
    run_experiments_all_datasets(args, log_dir, experiment_name="vanilla-convbert")

    # args.use_observers=True
    # args.mlm_pretraining=False
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers")

    # args.use_observers=True
    # args.mlm_pretraining=True
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+observers+mlm")

    # args.use_observers=True
    # args.mlm_pretraining=True
    # run_experiments_all_datasets(args, log_dir, experiment_name="convbert+mlm")














