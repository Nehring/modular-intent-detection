# train and evaluate convbert, convbert+observers, convbert + mlm pretraining, convbert + oberservers + mlm pretraining
# for all datasets

from experiment import init_experiments, run_single_experiment
import logging

def run_modify_modules(args, log_dir):

    for n_modules in [1,5,10,18]:
        name = f"n_modules={n_modules}"
        args.n_modules=n_modules
        logging.info("-"*20)
        logging.info(name)
        logging.info("-"*20)
        run_single_experiment(args, log_dir, experiment_name=name)

        # dname=dataset + "@10"
        # name = f"{experiment_name},n_modules={n_modules},dataset={dname}"
        # logging.info("-"*20)
        # logging.info(experiment_name)
        # logging.info("-"*20)
        # args.subsample_train_ten=True
        # run_single_experiment(args, log_dir, experiment_name=name)

if __name__ == '__main__':

    args, log_dir=init_experiments("modify_n_modules")
    args.dropout=0.0

    run_modify_modules(args, log_dir)














