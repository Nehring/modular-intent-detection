#!/bin/sh

export TORCH_HOME=/netscratch/nehring/cache/torch
export PIP_CACHE_DIR=/netscratch/nehring/cache/pip

cd /netscratch/nehring/projects/id-conversational-sentence-encoders

conda remove wrapt
pip install conversational-sentence-encoder==0.0.5