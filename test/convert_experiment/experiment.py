from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from conversational_sentence_encoder.vectorizers import SentenceEncoder
import pandas as pd
import random
from sklearn.metrics import f1_score, accuracy_score

random.seed(0)

subsample=False

# read the hwu64 data
def load_dataset():

    infile="NLU-Data-Home-Domain-Annotated-All.csv"
    df_home=pd.read_csv(infile, sep=";")

    # drop samples that have no text
    df_home=df_home[df_home["answer"].notna()]

    df_home=pd.DataFrame({
        "utterance": df_home["answer"],
        "intent": df_home["intent"],
        "scenario": df_home["scenario"],
        "source": "hwu64",
        "additional_info": None
    })

    def train_test_split():
        p=random.random()
        if p<0.5:
            return "train"
        elif p<0.75:
            return "valid"
        else:
            return "test"

    df_home["dataset"]=df_home.intent.apply(lambda x : train_test_split())
    train=df_home[df_home.dataset=="train"]
    valid=df_home[df_home.dataset=="valid"]
    test=df_home[df_home.dataset=="test"]

    if subsample:
        train=train.sample(n=100)
        valid=valid.sample(n=100)
        test=test.sample(n=100)
    return train, valid, test

df_train, df_valid, df_test=load_dataset()

sentence_encoder = SentenceEncoder(multiple_contexts=False)

def train():
    print("train...")
    X=list(df_train.utterance)
    y=list(df_train.intent)
    # initialize the ConveRT dual-encoder model

    # output 1024 dimensional vectors, giving a representation for each sentence. 
    X_encoded = sentence_encoder.encode_sentences(X)

    # encode labels
    le = preprocessing.LabelEncoder()
    y_encoded = le.fit_transform(y)

    # fit the KNN classifier on the toy dataset
    clf = KNeighborsClassifier(n_neighbors=len(df_train.intent.unique())).fit(X_encoded, y_encoded)
    return le, clf

le, clf=train()

def test():
    print("test...")
    X=list(df_train.utterance)
    y_true=list(df_train.intent)

    test = sentence_encoder.encode_sentences(X)
    prediction = clf.predict(test)

# this will give the intents ['SPEAK_HUMAN' 'RETURN']
    y_pred=le.inverse_transform(prediction)

    f1_macro=f1_score(y_true, y_pred, average="macro")
    print("f1 macro", f1_macro)

    f1_micro=f1_score(y_true, y_pred, average="micro")
    print("f1 micro", f1_micro)

    accuracy=accuracy_score(y_true, y_pred)
    print("accuracy", accuracy)

test()