import os
import json
import pandas as pd
import random

dialoglue_path="../../dialoglue/data_utils/dialoglue"
output_folder="dataset/"

# get hwu64 mapping of domains to intents
def make_hwu64_mapping():
    cats_file=os.path.join(dialoglue_path, "hwu/categories.json")
    intents=json.load(open(cats_file, "r"))
    mapping={}
    for intent in intents:
        domain=intent[0:intent.find("_")]
        mapping[intent]=domain
    return mapping

# get clinc150 mapping of domains to intents
def make_clinc_mapping():
    # the domains.json file originates from the clinc150 github repo:
    # https://raw.githubusercontent.com/clinc/oos-eval/master/data/domains.json
    domains=json.load(open("domains.json", "r"))
    mapping={}
    for domain, l in domains.items():
        for intent in l:
            mapping[intent]=domain
    return mapping

def make_glue_datasets():
    paths={
        "clinc": "dialoglue/data_utils/dialoglue/clinc",
        "hwu": "dialoglue/data_utils/dialoglue/hwu"
    }

    mappings={
        "clinc": make_clinc_mapping(),
        "hwu": make_hwu64_mapping()
    }

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    # create a single dataframe for each dataset with additional columns: dataset (train, val and test) and domain
    files=["train", "val", "test"]
    datasets={}
    for dataset in paths.keys():
        new_dataframe=[]
        subsets={}
        for f in files:
            df=pd.read_csv(os.path.join(dialoglue_path, dataset, f + ".csv"))
            df=df.rename(columns={"category": "intent"})
            df["domain"]=df.intent.apply(lambda x : mappings[dataset][x])
            df["dataset"]=f
            new_dataframe.append(df)
        new_dataframe=pd.concat(new_dataframe)
        datasets[dataset]=new_dataframe
        outfile=os.path.join(output_folder, dataset + ".csv")
        new_dataframe.to_csv(outfile)
        print(f"wrote{outfile}")

        domains=list(new_dataframe.domain.unique())
        outfile=os.path.join(output_folder, dataset + "_domains.json")
        print(f"wrote{outfile}")
        f=open(outfile, "w")
        f.write(json.dumps(domains))
        f.close()
    
    return datasets


# read the hwu64 data
def create_hwu64_dataframe():

    infile="NLU-Data-Home-Domain-Annotated-All.csv"
    df_home=pd.read_csv(infile, sep=";")

    # drop samples that have no text
    df_home=df_home[df_home["answer"].notna()]

    df_home=df_home[df_home.status.apply(lambda x : x not in ["IRR_XL", "IRR", "IRR_XR"])]

    df_home=pd.DataFrame({
        "text": df_home["answer"],
        "intent": df_home["intent"],
        "domain": df_home["scenario"],
    })

    df_home.intent = df_home.domain + "_" + df_home.intent

    intent_blacklist=["music_dislikeness", "general_greet", "audio_volume_other", "cooking_query"]
    df_home=df_home[df_home.intent.apply(lambda x : x not in intent_blacklist)]

    def assign_dataset():
        r=random.random()
        if r<0.8:
            return "train"
        elif r<0.9:
            return "val"
        else:
            return "test"
    df_home["dataset"]=df_home.intent.apply(lambda x : assign_dataset())

    outfile=os.path.join(output_folder, "hwu_orig.csv")
    df_home.to_csv(outfile)
    print(f"wrote{outfile}")

    domains=list(df_home.domain.unique())
    outfile=os.path.join(output_folder, "hwu_orig_domains.json")
    f=open(outfile, "w")
    f.write(json.dumps(domains))
    f.close()
    print(f"wrote{outfile}")

    return df_home

if __name__ == '__main__':

    datasets=make_glue_datasets()
    datasets["hwu_orig"]=create_hwu64_dataframe()

    # get statistics of datasets
    for dataset in datasets.keys():
        print("statistics for " + dataset)
        df=datasets[dataset]
        print(f"number of utterances: {len(df):,}")
        print(f"number of intents: {len(df.intent.unique()):,}")
        print(f"number of domains: {len(df.domain.unique()):,}")
        print()